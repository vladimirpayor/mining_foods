// Modules to control application life and create native browser window
const {app, BrowserWindow} = require('electron')
const {ipcMain} = require('electron')
const fs = require('fs');
// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow



function createTerminalWindow () {
  // Create the browser window.
  mainWindow = new BrowserWindow({
   frame: false,
   closable: false,
   fullscreen: true,
   alwaysOnTop: true,

    webPreferences: {
      nodeIntegration: true
    }
  })

  // and load the index.html of the app.
  mainWindow.loadFile('index.html')

  // Open the DevTools.
  // mainWindow.webContents.openDevTools()

  // Emitted when the window is closed.
  mainWindow.on('closed', function () {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    mainWindow = null
  })
}



function createCashierWindow() {
  cashierWindow = new BrowserWindow({

    minWidth: 600,
    minHeight: 350,
    closable: false,
    webPreferences: {
      nodeIntegration: true
    }
  })

  // 
  cashierWindow.loadFile('cashier.html');
  cashierWindow.focus();

  cashierWindow.setMenuBarVisibility(false)
  cashierWindow.on('closed', function () {
    
    cashierWindow = null;
   

  })

}

function initApp(){
  var openTerminalMode;
  if (fs.existsSync('./config.cnf')) {
    let rawdata = fs.readFileSync('config.cnf');

    config = JSON.parse(rawdata);
    openTerminalMode = config["terminal"];
    
} else {
  openTerminalMode = false;
    

}

if (openTerminalMode){
  createTerminalWindow();
 
} else {
  createCashierWindow();
}
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', initApp)


ipcMain.on('close-app', (evt, arg) => {
  app.exit(0)
})

ipcMain.on('cashier-mode', (evt, arg) => {
  mainWindow.destroy()
  createCashierWindow()
})

ipcMain.on('terminal-mode', (evt, arg) => {
  cashierWindow.destroy()
  createTerminalWindow()
})

// Quit when all windows are closed.
app.on('window-all-closed', function () {
  
})

app.on('activate', function () {
  // On macOS it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (mainWindow === null) createWindow()
})

