const SerialPort = require('serialport');
const Readline = require('@serialport/parser-readline');
const { ipcRenderer } = require('electron');
const https = require('https')
const si = require('systeminformation');
const fs = require('fs');
const configController = require('./configController.js');
const alertController = require('./alertController.js');
//при загрузке приложения проверяем состояние терминала и его статус, обновляем конфиг
window.onload = function() {
  initTerminal();
}


var board_serial;
var port;
var parser;


si.diskLayout()
    .then(data => {
      console.log(data[0].serialNum);
        board_serial = data[0].serialNum;
    });

setInterval(getStats(), 10000)
setInterval(checkTerminal(), 10000)

function getStats(){
  console.log("check")
  https.get('https://digital.spmi.ru/mining_foods_test/api/v1/service/terminal_stats/?uid='+configController.readUID()+'&token=OXsqAi0OXEWtZK9-ygwMdv7lgChBAz5stqUfUioVVE8', (resp) => {
    let data = '';
    // A chunk of data has been recieved.
    resp.on('data', (chunk) => {
      data += chunk;
    });
    // The whole response has been received. Rewrite token to config.
    resp.on('end', () => {
      console.log(resp.statusCode);
      if (resp.statusCode == 200) {
        terminal_all = JSON.parse(data).terminal_total_requests;
           terminal_today = JSON.parse(data).terminal_today_requests;
           document.getElementById("stats").textContent =`Статистика. Всего по системе за день: ${terminal_all}, Всего по терминалу за день: ${terminal_today}. В случае возникновения вопросов напишите нам: digital@spmi.ru`;
           
        return;
       
      }
    });

  }).on("error", (err) => {
    console.log("Error: " + err.message);
  });

}



function initTerminal() {
  configController.generateUID();
  checkTerminal();
  getStats();
  var config;
  try {
    if (fs.existsSync('./config.cnf')) {
      let rawdata = fs.readFileSync('config.cnf');

      config = JSON.parse(rawdata);
    } else {

      config = { port: "", token: "" }

    }
  } catch (err) {
    console.error(err)
  }
  if (config["port"] != "" && config["port"] != null) {

    port = new SerialPort(config["port"], { autoOpen: true });
    parser = port.pipe(new Readline({ delimiter: '\r\n' }));
    port.on("open", () => {
      console.log('serial port open');
    });
    //асинхронное чтение порта
    parser.on('data', data => {
      console.log('Данные порта:', data);
     
      card = parseCard(data);
      console.log(card);
      if (card != null && card!="no card") {
        getQuota(card, data);
      } 
       else if ( card!="no card"){
        
        alertController.alertBadCard();
        
      }

    });
  }
}

//закрыть приложение
document.querySelectorAll('.closeApp').forEach(item => {
  item.addEventListener('click', event => {
    ipcRenderer.send('close-app')
    return;
  });
});
//проверка подключения и статуса терминала на сервере
document.querySelectorAll('.redo').forEach(item => {
  item.addEventListener('click', event => {
    checkTerminal();
    return;
  });
});

//скрытая кнопка админки
document.querySelector('#btnAdmin').addEventListener('click', () => {
  openPinPad()

});

document.querySelector('#btnCashier').addEventListener('click', () => {
  port.close(function (err) {
    console.log('port closed', err);
});
  ipcRenderer.send('cashier-mode')

});


//регистрация терминала на сервере при первом подключении
//TODO: добавить алерт если терминал уже был зарегистирован
function registerTerminal() {
 //пишем UID в терминал
 configController.generateUID();
  


 
  https.get('https://digital.spmi.ru/mining_foods_test/api/v1/terminal/register/?uid='+configController.readUID(), (resp) => {
    let data = '';
    // A chunk of data has been recieved.
    resp.on('data', (chunk) => {
      data += chunk;
    });
    // The whole response has been received. Rewrite token to config.
    resp.on('end', () => {
      console.log(resp.statusCode);
      if (resp.statusCode == 200) {
        token = JSON.parse(data).token;
        id = JSON.parse(data).id;
        console.log(id)



        configController.updateConfig("active", token,  undefined, undefined,undefined, id )
        checkTerminal();

        return;
      } else {

        configController.updateConfig("unregistered", undefined, undefined,undefined, undefined, undefined )
        checkTerminal();

        return;

        //TODO: выбрасывать сообщение об ошибке

      }
    });



  }).on("error", (err) => {
    console.log("Error: " + err.message);
  });

}

//сообщение - ошибка подключения считывателя
function checkTerminal() {
  configController.generateUID()

  console.log("Checking terminal")


  const requset ='https://digital.spmi.ru/mining_foods_test/api/v1/terminal/status/?uid='+configController.readUID();
  console.log(requset);
  //запрос проверки статуса терминала у сервера
  https.get(requset, (resp) => {
    let data = '';
    // A chunk of data has been recieved.
    resp.on('data', (chunk) => {
      data += chunk;
    });
    // Шлем запрос на сервер
    resp.on('end', () => {
      console.log(resp.statusCode);
      if (resp.statusCode == 200) {
        status = JSON.parse(data).status;
        console.log(status);
        document.getElementById('id01').style.display = 'none';


        var config = {};
        //пытаемся читать конфиг, если его  нет то создаем новый
        try {
          if (fs.existsSync('./config.cnf')) {
            let rawdata = fs.readFileSync('config.cnf');
      
            config = JSON.parse(rawdata);
          } else {
      
            config = { port: "", token: "" }
      
          }
        } catch (err) {
          //TODO: кидать алерт о  проблеме с доступом к файлам и просьбой запустить от админа
          console.error(err);
          return;
        }

      
        //сбрасываем алерты если все хорошо

        if (status == "active") {
          document.getElementById('id05').style.display = 'none';
          document.getElementById('id04').style.display = 'none';

          //проверяем указан ли порт в конфиге если да - тестируем его, если нет - алерт

          //перезапись конфига
           configController.updateConfig("active", undefined, undefined);
          if (config["port"] == "") {
          alertReader()
            return;

          } else {
            checkReader(config["port"])
            return;

          }

        }
        //кидаем алерт если статус не рабочий
       else if (status == "unregistered") {
          console.log("U");

          alertUnregistered()

          configController.updateConfig(status, undefined, undefined);
          return;

        }
        if (status == "banned") {
          alertBanned();

          //перезапись конфигая
          configController.updateConfig(status, undefined, undefined);
          return;

        }

      } else {
        console.log(JSON.parse(data).message)
      }
    });

  }).on("error", (err) => {
    //ошибка связи с сервером
    document.getElementById('id01').style.display = 'block';


    console.log("Error: " + err.message);
  })
}

//проверка наличия ридера карт
//TODO: писать команду i в порт и проверять ответ
function checkReader(portTitle) {

  // port.open(function (err) {
  //   if (err) {
  //     return console.log('Error opening port: ', err.message)
  //   }

  //   // Because there's no callback to write, write errors will be emitted on the port:
  //   port.write('i\n');
  // })

  // port.on('data', function (data) {
  console.log('Data:', portTitle)
  // })

  return;
}

function getQuota(card, card_data) {

  var config = {};
  //пытаемся читать конфиг, если его  нет то создаем новый
  try {
    if (fs.existsSync('./config.cnf')) {
      let rawdata = fs.readFileSync('config.cnf');

      config = JSON.parse(rawdata);
    } else {

      config = { port: "", status: "", token: "" }

    }
  } catch (err) {
    //TODO: кидать алерт о  проблеме с доступом к файлам и просьбой заупстить от админа
    console.error(err);
    return;
  }
  var uid = configController.readUID();


  if (config["token"] != null && config["token"] != "") {
    https.get('https://digital.spmi.ru/mining_foods_test/api/v1/clients/pay?token=' + config["token"] + '&uid=' + uid + '&client_card_id=' + card+'&card_data=' + card_data, (resp) => {
      let data = '';
      // A chunk of data has been recieved.
      resp.on('data', (chunk) => {
        data += chunk;
      });
      // The whole response has been received. Rewrite status.
      resp.on('end', () => {
        console.log(data)
        if (resp.statusCode == 200) {
          approved = JSON.parse(data).approved;


           user_name = JSON.parse(data).user_name;
           logs = JSON.parse(data).logs;
           quota = JSON.parse(data).quota;
           terminal_all = JSON.parse(data).terminal_total_requests;
           terminal_today = JSON.parse(data).terminal_today_requests;
           document.getElementById("stats").textContent =`Статистика. Всего по системе за день: ${terminal_all}, Всего по терминалу за день: ${terminal_today}. В случае возникновения вопросов напишите нам: digital@spmi.ru`;
           
          console.log(approved);
          console.log(JSON.parse(data).message);
          //проверяем что квота не исчерпана
          if (approved == true) {
          
           
              last_log = logs[logs.length-1];
              used = logs.length;
              group = JSON.parse(data).group;
              let price_off = JSON.parse(data).price_off;
              let user_id = JSON.parse(data).user_id;
              let payment_id=JSON.parse(data).payment_id;

              group_id = JSON.parse(data).group_id;
              first_time = JSON.parse(data).first_time;
           
              


           //если обратились первый раз - зеленый экран
           
              
          // alertController.alertApproved(user_name,quota, used, group, group_id, first_time);
            //платежка
            console.log(data);
            alertController.callPayment(user_id,payment_id,group_id, price_off);
            
          } else {
            alertController.alertDeclined(user_name, quota);
            return;
          }


        } else {

       
           
          alertController.alertMsg = JSON.parse(data).message;
          console.log(JSON.parse(data).message);
          alertController.alertClientError(alertController.alertMsg);
          return;

        }
      });



    }).on("error", (err) => {
      console.log("Error: " + err.message);
    });

  }
}


//открыть панель ввод пин-кода
function openPinPad() {
  console.log("open PINPAND")
  let pin = "";
  updatePINIndicator("");

  document.getElementById('pinPad').style.display = 'block';
  console.log("open PINPAND")

  document.querySelector('#closePin').addEventListener('click', () => {
    document.getElementById('pinPad').style.display = 'none';
    checkTerminal();
    return;
  });

  document.querySelectorAll('.num').forEach(item => {
    item.addEventListener('click', event => {
      number = item.getAttribute("number");
      console.log(number);
      document.getElementById("wrong").style.display = "none";
      if (pin.length < 4) {
        pin += number;
        updatePINIndicator(pin);
        console.log(pin);
      }
      if (pin.length == 4) {


        https.get('https://digital.spmi.ru/mining_foods_test/api/v1/terminal/login/?pin=' + pin, (resp) => {
          let data = '';
          // A chunk of data has been recieved.
          resp.on('data', (chunk) => {
            data += chunk;
          });
          // The whole response has been received. Rewrite status.
          resp.on('end', () => {
            if (resp.statusCode == 200) {
              approved = JSON.parse(data).approved;

              console.log(approved);
              console.log(JSON.parse(data).message);

              if (approved == true) {
                console.log("open admin")
                document.getElementById('pinPad').style.display = 'none';
                pin = "";
                updatePINIndicator("");

                openAdmin();
              } else {
                document.getElementById("wrong").style.display = "block";
                pin = "";
                updatePINIndicator("");
              }


            } else {
              console.log(JSON.parse(data).message)

            }
          });



        }).on("error", (err) => {
          console.log("Error: " + err.message);
        });
      }
    })
  });

  document.querySelector('#del').addEventListener('click', () => {
    document.getElementById("wrong").style.display = "none";
    if (pin.length > 1) {
      pin = pin.substring(0, pin.length - 1);
      updatePINIndicator(pin);
    } else {
      pin = "";
      updatePINIndicator(pin);
    }
    console.log(pin);


  });

  document.querySelector('#clr').addEventListener('click', () => {
    pin = "";
    updatePINIndicator(pin);
    document.getElementById("wrong").style.display = "none";
    console.log(pin);
  });


}
//обновляет строку с пином 
//TODO: пофиксить баг с обновлением строки после закрытия окна,
// если было введено только 2-3 числа
function updatePINIndicator(num) {


  if (num.length < 1 || num == NaN) {

    document.getElementById("pin1").classList.remove("fas");
    document.getElementById("pin1").classList.add("far");


    document.getElementById("pin2").classList.remove("fas");
    document.getElementById("pin2").classList.add("far");

    document.getElementById("pin3").classList.remove("fas");
    document.getElementById("pin3").classList.add("far");

    document.getElementById("pin4").classList.remove("fas");
    document.getElementById("pin4").classList.add("far");




  }

  if (num.length == 1) {

    document.getElementById("pin2").classList.remove("fas");
    document.getElementById("pin2").classList.add("far");

    document.getElementById("pin1").classList.remove("far");
    document.getElementById("pin1").classList.add("fas");


  }

  if (num.length == 2) {

    document.getElementById("pin3").classList.remove("fas");
    document.getElementById("pin3").classList.add("far");

    document.getElementById("pin2").classList.remove("far");
    document.getElementById("pin2").classList.add("fas");

  }

  if (num.length == 3) {

    document.getElementById("pin4").classList.remove("fas");
    document.getElementById("pin4").classList.add("far");

    document.getElementById("pin3").classList.remove("far");
    document.getElementById("pin3").classList.add("fas");

  }

  if (num.length == 4) {
    document.getElementById("pin4").classList.remove("far");
    document.getElementById("pin4").classList.add("fas");

  }

}

//открыть панель управления
function openAdmin() {
  document.getElementById('id03').style.display = 'block';

  SerialPort.list().then(ports => {
    document.getElementById("port-list").innerHTML = `${ports.map(port => `<option value="${port.comName}">${port.comName}</option>`).join('')}`
  })

  //проверка при закрытии
    document.querySelector('#closeAdmin').addEventListener('click', () => {
    document.getElementById("portData").textContent = ``;
    document.getElementById('id03').style.display = 'none';
    initTerminal()
    return;
  });

  document.querySelector('#saveAdmin').addEventListener('click', () => {
    document.getElementById("portData").textContent = ``;
    document.getElementById('id03').style.display = 'none';
    initTerminal()
    return;
  });



  document.getElementById("port-list").addEventListener('change', (port_title) => {
    const result = document.getElementById("portName");
    const openPortBtn = document.getElementById("startBtn");
    result.textContent = `Активный порт: ${port_title.target.value}`;
    
    openPortBtn.style.display = "block";

  });

  const openPortBtn = document.getElementById("startBtn");

  openPortBtn.addEventListener('click', function () {
    configController.updateConfig(null, null, document.getElementById("port-list").value);
    checkReader(document.getElementById("port-list").value);
    document.getElementById("portData").textContent = `Порт переназначен`;

  });

}


  //сообщение - терминал заблокирован на сервере
  function alertBanned() {
    document.getElementById('id05').style.display = 'block';
    document.querySelector('.redo').addEventListener('click', () => {
      checkTerminal();
    });
  }
  //сообщение - терминал не зарегистрирован на сервере
  function alertUnregistered() {
    document.getElementById('id04').style.display = 'block';
    document.querySelector('#btnReg').addEventListener('click', () => {
  
      registerTerminal();
      
      return;
  
    });
  
  }


  //сообщение - ошибка подключения считывателя
  function alertReader() {
    document.getElementById('alertReader').style.display = 'block';
    document.querySelector('#connect').addEventListener('click', () => {
  
      document.getElementById('alertReader').style.display = 'none';
      openPinPad();
      return;
    });
  
    console.log("Reader")
  
  }

//парсер карты 
function parseCard(data) {
  var msg;
  var res = data.match(/HID/g);
  var nocard = data.match(/No/g);
  if (res == "HID") {
    msg = data.split(' ').slice(-1).join(' ');
    return msg;
  } 
  if (nocard == "No") {
    
    return "no card";
  } 

  return msg;
  
}














