const si = require('systeminformation');
const fs = require('fs');


si.diskLayout()
    .then(data => {
      console.log(data[0].serialNum);
        board_serial = data[0].serialNum;
    });

var board_serial;
//обновление конфига
const configController = require('./configController.js');
function updateConfig(status, token, port, uid, terminal, id) {
    var config = {};
    console.log("update config")
    //пытаемся читать конфиг, если его  нет то создаем новый
    try {
      if (fs.existsSync('./config.cnf')) {
        let rawdata = fs.readFileSync('config.cnf');
  
        config = JSON.parse(rawdata);
      } else {
  
        config = { port: "", status: "", token: "", uid: "", terminal: false, id: "" }
  
      }
    } catch (err) {
      //TODO: кидать алерт о  проблеме с доступом к файлам и просьбой заупстить от админа
      console.error(err);
      return;
    }
   
    //
    if (status != null) {
      config.status = status;
  
    }
    if (token != null) {
      config.token = token;
    }
    if (port != null) {
      config.port = port;
  
    }
  
    if (uid != null) {
      config.uid = uid;
  
    }

    if (uid != null) {
        config.uid = uid;
    
      }

      if (terminal != null) {
        config.terminal = terminal;
    
      }

      if (id != null) {
        config.id = id;
    
      }
  
    //перезапись конфига
   
    let conf_data = JSON.stringify(config);
    fs.writeFileSync('config.cnf', conf_data);
    return;
  
  }
  

  function generateUID(){
    configController.updateConfig(undefined, undefined, undefined, board_serial, undefined, undefined);
  }
  
  function readUID(){
  
    let rawdata = fs.readFileSync('config.cnf');
        
    config = JSON.parse(rawdata);
  
    return config["uid"];
  
  }


  function readToken(){
  
    let rawdata = fs.readFileSync('config.cnf');
        
    config = JSON.parse(rawdata);
  
    return config["token"];
  
  }

module.exports.updateConfig = updateConfig; 
module.exports.readUID = readUID;
module.exports.readToken = readToken;
module.exports.generateUID = generateUID;