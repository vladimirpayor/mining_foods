const { ipcRenderer } = require('electron');
const https = require('https');
const fs = require('fs');
const configController = require('./configController.js');
//var Chart = require('chart.js');
const si = require('systeminformation');
const SerialPort = require('serialport');
const Readline = require('@serialport/parser-readline');
const alertController = require('./alertController.js');




window.onload = function () {
   
    
    initTerminal()
}
document.querySelector('#btnHome').addEventListener('click', () => {
    checkTerminal();
    document.getElementById("mainView").style.display = "block";
    document.getElementById("settingsView").style.display = "none";
});

    document.querySelector('#btnSettings').addEventListener('click', () => {
     document.getElementById("settingsView").style.display = "block";
     document.getElementById("mainView").style.display = "none";
    SerialPort.list().then(ports => {
        document.getElementById("port-list").innerHTML = `${ports.map(port => `<option value="${port.comName}">${port.comName}</option>`).join('')}`
    })

    document.getElementById("port-list").addEventListener('change', (port_title) => {
        const result = document.getElementById("portName");

        result.textContent = `Активный порт: ${port_title.target.value}`;

    });

    const openPortBtn = document.getElementById("startBtn");

    openPortBtn.addEventListener('click', function () {
        configController.updateConfig(null, null, document.getElementById("port-list").value);
        document.getElementById("noPort").textContent = ``;
        document.getElementById("portData").textContent = `Порт переназначен`;
        initTerminal();

    });
});


si.diskLayout()
    .then(data => {
      console.log(data[0].serialNum);
        board_serial = data[0].serialNum;
    });

document.querySelector('#btnCashier').addEventListener('click', () => {
    port.close(function (err) {
        console.log('port closed', err);
    });
    ipcRenderer.send('terminal-mode')

});

document.querySelector('#saveAdmin').addEventListener('click', () => {

 
    checkTerminal();
    var defaultMode = document.getElementById("defaultMode").checked;
    console.log(defaultMode)
    configController.updateConfig(undefined, undefined, undefined,undefined,defaultMode , undefined)
        document.getElementById("mainView").style.display = "block";
    document.getElementById("settingsView").style.display = "none";
});

// document.querySelector('#btnStatistics').addEventListener('click', () => {
//     checkTerminal()
//     document.getElementById("settingsView").style.display = "none";
//     document.getElementById("statisticsView").style.display = "block";
//     getStatistics();



// });






document.querySelectorAll('.closeApp').forEach(item => {
    item.addEventListener('click', event => {
        ipcRenderer.send('close-app')
        return;
    });
});

// var doughnut = document.getElementById('usersChart').getContext('2d');

// var usersDoughnutChart = new Chart(doughnut, {
//     type: 'doughnut',
//     data: data = {
//         labels: [
//             'Cтуденты',
//             'Yellow',
//             'Blue'
//         ],
//         datasets: [{
//             data: [100, 0, 0],


//             // These labels appear in the legend and in the tooltips when hovering different arcs

//             backgroundColor: [
//                 'rgba(0, 255, 12, 0.9)',
//                 'rgba(54, 162, 235, 0.2)',
//                 'rgba(75, 192, 192, 0.2)',
//                 'rgba(153, 102, 255, 0.2)',
//                 'rgba(255, 159, 64, 0.2)'
//             ],

//         }],
//     },

// });


// var ctx = document.getElementById('visitsChart').getContext('2d');
// var myChart = new Chart(ctx, {
//     type: 'bar',
//     data: {
//         labels: ['Red'],
//         datasets: [{
//             label: '# of Votes',
//             data: [12, 19, 3, 5, 2, 3],

//             backgroundColor: [
//                 'rgba(255, 99, 132, 0.2)',
//                 'rgba(54, 162, 235, 0.2)',
//                 'rgba(255, 206, 86, 0.2)',
//                 'rgba(75, 192, 192, 0.2)',
//                 'rgba(153, 102, 255, 0.2)',
//                 'rgba(255, 159, 64, 0.2)'
//             ],
//             borderColor: [
//                 'rgba(255, 99, 132, 1)',
//                 'rgba(54, 162, 235, 1)',
//                 'rgba(255, 206, 86, 1)',
//                 'rgba(75, 192, 192, 1)',
//                 'rgba(153, 102, 255, 1)',
//                 'rgba(255, 159, 64, 1)'
//             ],
//             borderWidth: 1
//         }]
//     },
//     options: {
//         scales: {
//             yAxes: [{
//                 ticks: {
//                     beginAtZero: true
//                 }
//             }],
//             xAxes: [{
//                 type: 'time',
//                 time: {
//                     unit: 'minute'
//                 }
//             }]

//         }
//     }
// });


function initTerminal() {

    checkTerminal();
    var config;
    try {
        if (fs.existsSync('./config.cnf')) {
            let rawdata = fs.readFileSync('config.cnf');

            config = JSON.parse(rawdata);
        } else {

            config = { port: "", token: "" }

        }
    } catch (err) {
        console.error(err)
    }
    if (config["port"] != "" && config["port"] != null) {

        port = new SerialPort(config["port"], { autoOpen: true });
        parser = port.pipe(new Readline({ delimiter: '\r\n' }));
        port.on("open", () => {
            console.log('serial port open');
        });
        //асинхронное чтение порта
        parser.on('data', data => {
            console.log('Данные порта:', data);
            

            card = parseCard(data);
            console.log(card);
            if (card != null && card != "no card") {
                getQuota(card, data);
            }
            else if (card != "no card") {

                alertController.alertBadCard();

            }

        });
    } else {
        document.getElementById("noPort").textContent = "Считыватель не подключен"
    }
}


function checkTerminal() {   
    configController.generateUID();

    console.log("Checking terminal")


    const requset = 'https://digital.spmi.ru/mining_foods_test/api/v1/terminal/status/?uid=' + configController.readUID();
    console.log(requset);
    //запрос проверки статуса терминала у сервера
    https.get(requset, (resp) => {
        let data = '';
        // A chunk of data has been recieved.
        resp.on('data', (chunk) => {
            data += chunk;
        });
        // Шлем запрос на сервер
        resp.on('end', () => {
            console.log(resp.statusCode);
            if (resp.statusCode == 200) {
                webstatus = JSON.parse(data).status;
                console.log(webstatus);



                try {
                    if (fs.existsSync('./config.cnf')) {
                        let rawdata = fs.readFileSync('config.cnf');

                        config = JSON.parse(rawdata);
                        status = config.status;

                        const terminalStatus = document.getElementById("terminalStatus");
                        const terminalUID = document.getElementById("terminalUID");

                        if (webstatus == "active") {
                            console.log(status)

                            terminalStatus.innerHTML = "<p class='w3-text-green'>Терминал активен <i class='fas fa-check-circle w3-text-green'></i></p> ";
                            configController.updateConfig("active", undefined, undefined);
                        }
                        if (webstatus == "banned") {
                            configController.updateConfig("banned", undefined, undefined);

                            terminalStatus.innerHTML = " <p class='w3-text-red'>Терминал заблокирован <i class='fas fa-lock w3-text-red'></i></p> ";
                            var alert = document.getElementById("alertBanned");

                            // Add the "show" class to DIV
                            alert.className = "show";


                        }

                        if (webstatus == "unregistered") {
                           
                            document.getElementById("regButton").style.display = "block";
                            document.querySelector('#regButton').addEventListener('click', () => {
                                registerTerminal();

                            });
                            terminalStatus.innerHTML = " <p class='w3-text-red'>Терминал не зарегистрирован  <span class='fa-stack'> <i class='fas fa-cloud fa-stack-1x' ></i> <i class='fas fa-slash fa-stack-1x' ></i></span></p> ";

                        }


                        if (config["uid"] != null && config["uid"] != "") {

                            terminalUID.textContent = `UID терминала: ${config["uid"]}`;

                        }


                        document.getElementById("defaultMode").checked = config["terminal"];





                    }
                } catch (err) {

                    console.error(err);
                    return;
                }



            } else {
                console.log(JSON.parse(data).message)
            }
        });

    }).on("error", (err) => {
        //ошибка связи с сервером
        document.getElementById('id01').style.display = 'block';


        console.log("Error: " + err.message);
    })
}


async function getStatistics(){
    var terminal = {
        "date_begin": "25-04-2020",
    "date_end": "25-04-2020",
    "terminal_id": 127
    }
    let response = await fetch('https://digital.spmi.ru/mining_foods_test/api/v1/service/logs?token=OXsqAi0OXEWtZK9-ygwMdv7lgChBAz5stqUfUioVVE8', {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json;charset=utf-8'
        },
        body: JSON.stringify(terminal)
      });

      let result = await response.json();
      console.log(result)

}



function registerTerminal() {
    //пишем UID в терминал
    configController.generateUID();
    document.getElementById("regButton").style.display = "none";




    https.get('https://digital.spmi.ru/mining_foods_test/api/v1/terminal/register/?uid=' + configController.readUID(), (resp) => {
        let data = '';
        // A chunk of data has been recieved.
        resp.on('data', (chunk) => {
            data += chunk;
        });
        // The whole response has been received. Rewrite token to config.
        resp.on('end', () => {
            console.log(resp.statusCode);
            if (resp.statusCode == 200) {
                token = JSON.parse(data).token;
                id = JSON.parse(data).id;
                console.log(id)
   
    

                configController.updateConfig("active", token,  undefined, undefined,undefined, id )
                checkTerminal();

                return;
            } else {

                configController.updateConfig("unregistered", undefined, undefined)
                checkTerminal();

                return;

                //TODO: выбрасывать сообщение об ошибке

            }
        });



    }).on("error", (err) => {
        console.log("Error: " + err.message);
    });

}


//парсер карты 
function parseCard(data) {
    var msg;
    var res = data.match(/HID/g);
    var nocard = data.match(/No/g);
    if (res == "HID") {
      msg = data.split(' ').slice(-1).join(' ');
      return msg;
    } 
    if (nocard == "No") {
      
      return "no card";
    } 
  
    return msg;
    
  }
  
  
  
  function getQuota(card, card_data) {

    var config = {};
    //пытаемся читать конфиг, если его  нет то создаем новый
    try {
      if (fs.existsSync('./config.cnf')) {
        let rawdata = fs.readFileSync('config.cnf');
  
        config = JSON.parse(rawdata);
      } else {
  
        config = { port: "", status: "", token: "" }
  
      }
    } catch (err) {
      //TODO: кидать алерт о  проблеме с доступом к файлам и просьбой заупстить от админа
      console.error(err);
      return;
    }
    var uid = configController.readUID();
  
  
    if (config["token"] != null && config["token"] != "") {
      https.get('https://digital.spmi.ru/mining_foods_test/api/v1/clients/pay?token=' + config["token"] + '&uid=' + uid + '&client_card_id=' + card+'&card_data=' + card_data, (resp) => {
        let data = '';
        // A chunk of data has been recieved.
        resp.on('data', (chunk) => {
          data += chunk;
        });
        // The whole response has been received. Rewrite status.
        resp.on('end', () => {
          if (resp.statusCode == 200) {
            approved = JSON.parse(data).approved;
  
             //разобрать сообщение
             console.log(JSON.parse(data).user_name);
             console.log(JSON.parse(data).logs);
             user_name = JSON.parse(data).user_name;
             logs = JSON.parse(data).logs;
             quota = JSON.parse(data).quota;
             terminal_all = JSON.parse(data).terminal_total_requests;
             terminal_today = JSON.parse(data).terminal_today_requests;
             used = logs.length;
             
            console.log(approved);
            console.log(JSON.parse(data).message);
            //проверяем что квота не исчерпана
            if (approved == true) {
              console.log(logs[logs.length-1]);
                last_log = logs[logs.length-1];
                
                group = JSON.parse(data).group;

                group_id = JSON.parse(data).group_id;
                first_time = JSON.parse(data).first_time;
                
             //если обратились первый раз - зеленый экран
             alertController.alertApproved(user_name,quota, used, group, group_id, first_time);

             if (first_time){
            
             var icon = 'images/approved_professor.png'
             if (group_id == 1 ){
                icon = 'images/approved_student.png'
             }

             let myNotification = new Notification('Одобрено', {
                body: user_name+ ". квота: "+quota+", использовано: "+used,
                icon:  icon,
              })
              return;  
            } else {
                var icon = 'images/on_hold.png'
                let myNotification = new Notification('Одобрено - повторное считывание', {
                    body: user_name+ ". квота: "+quota+", использовано: "+used,
                    icon:  icon,
                  })
                  return;
            }
              
            } else {
                var icon = 'images/declined.png'
                let myNotification = new Notification('Отказ', {
                    body: user_name+ ". квота: "+quota+", использовано: "+used +" - квота исчерапана.",
                    icon:  icon,
                  })
              alertController.alertDeclined(user_name, quota);
              return;
            }
  
  
          } else {
             
            alertController.alertMsg = JSON.parse(data).message;
            console.log(JSON.parse(data).message);
            let myNotification = new Notification('Ошибка', {
                body: JSON.parse(data).message,
                icon:  'images/declined.png'
              })
            alertController.alertClientError(alertController.alertMsg);
            return;
  
          }
        });
  
  
  
      }).on("error", (err) => {
        console.log("Error: " + err.message);
      });
  
    }
  }
  