const SerialPort = require('serialport');
const Readline = require('@serialport/parser-readline');

class PortController {
    constructor() {
        this.path = null;
        this.port = null;
        
    }

    open(path) {
        if(this.isOpen) {
          throw new Error('Port already open')
        }
        this.path = path
        this.port = new SerialPort(path)
      }


    close() {
        if (this.port) {
            this.port.close()
          }
    }
}
module.exports = PortController