const https = require('https')
const configController = require('./configController.js');
const { once } = require('process');
let paymentID = "";
let user_id = "";
let total_sum = "0"
//сообщение - ошибка подключения считывателя
function alertBadCard() {
    document.getElementById('alertCard').style.display = 'block';
    setTimeout(function(){hideAlert("alertCard");}, 3000);
    return;
  }
  
  function hideAlert(alert){
   console.log("hidden")
    document.getElementById(alert).style.display = 'none';

  }
  
  
  function alertDeclined(user_name, quota){
  
    document.getElementById('alertDeclined').style.display = 'block';
    document.getElementById("declinedUser").textContent =`Пользователь: ${user_name}`;
    document.getElementById("declinedQuota").textContent =`Квота ${quota} раз(а) в день исчерпана.`;
  
    setTimeout(function(){hideAlert("alertDeclined");}, 3000);
  
    
  }


  function callPayment(userid, paymentid, group_id, price_off){
    console.log("userID: "+userid);
    
    user_id = userid;
    paymentID = paymentid;
    console.log(price_off);

    if (price_off){
      document.getElementById('price_off').style.display = 'block';
    
    } else {
      document.getElementById('price_off').style.display = 'none';
     

    }

    document.getElementById('payment').style.display = 'block';
    let sum = "0";

    document.querySelectorAll('.pnum').forEach(item => {
      item.addEventListener('click', event => {
        number = item.getAttribute("number");
        if (sum == "0"){
          sum = "";
        }
        if (sum.length < 3) {
          sum += number;
          document.getElementById("sum").textContent = sum + " Pyб.";
        }
    });
  });


  document.querySelector('#closePayment').addEventListener('click', () => {
  
      sum ="0";
      document.getElementById("sum").textContent = sum + " Pyб.";
      document.getElementById('payment').style.display = 'none';

      
      return;
    });



    document.querySelector('#pclr').addEventListener('click', () => {
      sum = "0";
      document.getElementById("sum").textContent = sum + " Pyб."
      
    });
    
    document.querySelector('#pdel').addEventListener('click', () => {
      if (sum != "0"){
        console.log(sum);
      sum = sum.substring(0, sum.length - 1);
      document.getElementById("sum").textContent = sum + " Pyб.";
      } else {
        sum="0"
        document.getElementById("sum").textContent = sum + " Pyб.";
      }
      
    });

    document.querySelector('#declinePayment').addEventListener('click', () => {
      sum="0";
      document.getElementById("sum").textContent = sum + " Pyб.";
      document.getElementById('payment').style.display = 'none';
    });

    document.querySelector('#pay').addEventListener('click',()=>{   
      total_sum = sum; sum ="0";  
     document.getElementById("sum").textContent = sum + " Pyб.";
      paymentProcessing();}, {once: true});

  
}


function paymentProcessing() {

  https.get('https://digital.spmi.ru/mining_foods_test/api/v1/clients/payment?uid='+configController.readUID()+'&token='+configController.readToken()+'&sum='+
  total_sum+ '&paymentID='+paymentID+'&user_id='+user_id, (resp) => {
    let data = '';
    // A chunk of data has been recieved.
    resp.on('data', (chunk) => {
      data += chunk;
    });
    // The whole response has been received. Rewrite token to config.
    resp.on('end', () => {
      console.log(resp.statusCode);
      document.querySelector('#pay').removeEventListener('click',()=>{});
      if (resp.statusCode == 200) {
        
      
        alertApprovedPayment();
        return;
       
      } 
      if (resp.statusCode == 400) {
        //show error
        total_sum="0";
        document.getElementById("sum").textContent = sum + " Pyб.";
        document.getElementById('payment').style.display = 'none';
        let errorMsg = JSON.parse(data).message;
        alertClientError(errorMsg);
        return;
      }
    });

  }).on("error", (err) => {np
    console.log("Error: " + err.message);
  });
}
  
  function alertClientError(alertMsg){
    document.getElementById('alertClient').style.display = 'block';
    document.getElementById("clientError").textContent = `Сообщение: ${alertMsg}`;
    setTimeout(function(){hideAlert("alertClient");}, 3000);
  }
  

  function alertApprovedPayment(){
    sum="0";
    document.getElementById("sum").textContent = sum + " Pyб.";
    document.getElementById('payment').style.display = 'none';
    document.getElementById('alertApprovedPayment').style.display = 'block';
    setTimeout(function(){hideAlert("alertApprovedPayment");}, 3000);
  }
  
  function alertApproved(user_name,quota, used, group, group_id, first_time){
     
      //сбрасываем другие экраны
     document.getElementById("approvedCard").classList.replace("w3-ios-orange", "w3-green");
     document.getElementById("approvedCard").classList.replace("w3-ios-dark-blue", "w3-green");
     console.log(group_id)
     //меняем цвет для студентов
     if (group_id == 1){
      document.getElementById("approvedCard").classList.replace("w3-green","w3-ios-dark-blue" );
  
  
     }
  
     //если не первый раз за 5 минут приложили чипку - желтый экранчик
    if (!first_time){
      document.getElementById("approvedCard").classList.remove("w3-ios-dark-blue");
      document.getElementById("approvedCard").classList.remove("w3-green");
      document.getElementById("approvedCard").classList.add("w3-ios-orange");
    }
  
    document.getElementById('alertApproved').style.display = 'block';
    document.getElementById("bonAppetit").textContent = `Приятного аппетита, ${user_name.split(' ').slice(1, 3).join(' ')}!`;
    document.getElementById("ApprovedInfo1").textContent =`Квота питания: ${quota} раз(а) в день`;
    document.getElementById("ApprovedInfo2").textContent =`Использовано: ${used}`;
    document.getElementById("ApprovedInfo3").textContent =`Группа: ${group}`;
    setTimeout(function(){hideAlert("alertApproved");}, 5000);
  
  }
  
  


  module.exports.alertBadCard =  alertBadCard;
  module.exports.callPayment =  callPayment;
  module.exports.alertApproved =  alertApproved;

  module.exports.alertDeclined =  alertDeclined;
  module.exports.alertClientError =  alertClientError;
